//
//  MenuController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 3/16/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//  Changes Copyright © 2020 by Breastfeeding's Best Friend. All rights reserved.
//

import UIKit
import CoreData

class MenuController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Loaded MenuController")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        print("Received memory warning in MenuController")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

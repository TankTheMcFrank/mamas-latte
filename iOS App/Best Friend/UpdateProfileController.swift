//
//  UpdateProfileController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 8/1/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//

import CoreData
import UIKit

class UpdateProfileController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var genderSegmentedControl: UISegmentedControl!
    @IBOutlet weak var datePickerTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
            self.nameTextField.delegate = self
            if profile.childName == "" {
                self.nameTextField.placeholder = "Name"
            } else {
                self.nameTextField.placeholder = profile.childName
            }
            if profile.profileImage == 0 {
                self.genderSegmentedControl.selectedSegmentIndex = 0
            } else if profile.profileImage == 1 {
                self.genderSegmentedControl.selectedSegmentIndex = 1
            } else { //profile.profileImage == 2
                self.genderSegmentedControl.selectedSegmentIndex = 2
            }
            
            if profile.birthDateString == nil {
                datePickerTitle.text = "Due Date:"
                let dateFormatter: DateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM dd, yyyy HH:mm"
                datePicker.date = dateFormatter.date(from: profile.dueDateString!)!
            } else {
                datePickerTitle.text = "Birth Date:"
                let dateFormatter: DateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM dd, yyyy HH:mm"
                datePicker.date = dateFormatter.date(from: profile.birthDateString!)!
            }
            
        } else {
            print("Error getting profile object in viewDidLoad in UpdateProfileController.swift")
        }
        
        //Set background image
        self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Background3.png")!)
    }
    
    //Hide the keyboard when the return key is pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //Hide the keyboard when screen is touched
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //Once the update button gets pressed, we need to remove the current Profile object from Core Data
    //and replace it with a new Profile object that reflects the update Due Date/DOB
    @IBAction func updateButtonDidGetPressed(_ sender: Any) {
        var birthDate: Date = datePicker.date
        
        birthDate = birthDate.set9AMTime()!
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy HH:mm"
        
        var image: Int
        if (genderSegmentedControl.selectedSegmentIndex == 0) {
            image = 0
        } else if (genderSegmentedControl.selectedSegmentIndex == 1) {
            image = 1
        } else if (genderSegmentedControl.selectedSegmentIndex == 2) {
            image = 2
        } else {    //There was an error (somehow) with the imageSegmentedControl
            image = 1
        }
        
        if let profile = DatabaseController.getProfileObjectFromCoreData() {
            if nameTextField.text == "" { //They didn't change the name, so use the placeholder text for the child's name
                profile.childName = nameTextField.placeholder!
                profile.profileImage = Int64(image)
                if profile.birthDateString != nil {
                    profile.birthDateString = dateFormatter.string(from: birthDate)
                } else {
                    profile.dueDateString = dateFormatter.string(from: birthDate)
                }
            } else { //They changed the child's name, so use the new name
                profile.childName = nameTextField.text!
                profile.profileImage = Int64(image)
                if profile.birthDateString != nil {
                    profile.birthDateString = dateFormatter.string(from: birthDate)
                } else {
                    profile.dueDateString = dateFormatter.string(from: birthDate)
                }
            }
        }
        
        DatabaseController.saveContext()
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: updateProfileKey), object: self)
        print("Update Profile Notification Sent")
        
        displayProfileInfoToTerminal()
        
        dismiss(animated: true, completion: nil)
    }
    
    //Just dismiss the view if the cancel button gets pressed... do not alter any data
    @IBAction func cancelButtonDidGetPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func displayProfileInfoToTerminal() {
        if let profile = DatabaseController.getProfileObjectFromCoreData() {
            print("Baby Name: \(String(describing: profile.childName))")
            print("Image Selection: \(profile.profileImage)")
            print("Due Date: \(String(describing: profile.dueDateString))")
            print("Birth Date: \(String(describing: profile.birthDateString))")
        } else {
            print("ERROR: No profile object able to be retrieved from DatabaseController in displayProfileInforToTerminal() in UpdateProfileController.swift")
        }
    }
    
//    //This function gets called when a notification needs to be sent
//    @objc func updateProfileWhenNotify() {
//        print("Sent notification from UpdateProfile Controller")
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

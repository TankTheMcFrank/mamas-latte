//
//  AboutUsController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 3/16/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//  Changes Copyright © 2020 by Breastfeeding's Best Friend. All rights reserved.
//

import UIKit

class AboutUsController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var menuBarButton: UIBarButtonItem!
    @IBOutlet weak var aboutUsLabel: UILabel!
    @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var image1: UIImageView!
    
    let aboutUsMessage: NSMutableAttributedString = NSMutableAttributedString(string: "TJ is an International Board Certified Lactation Consultant (IBCLC) with nearly 20 years experience working with breastfeeding couplets.  She co-founded an in-home breastfeeding consultation service to help struggling families overcome breastfeeding challenges. TJ also successfully nursed both her children. She is excited to share her knowledge and experience for free with every parent with a smartphone who wants to provide breastmilk for their baby.\n\nKristina, a military spouse and working mother, just completed her breastfeeding journey with her son. She created a breastfeeding support group on Davis-Monthan Air Force Base to help moms achieve their breastfeeding goals through biweekly meetings and social media support.  Kristina is excited to support and inform all moms and caregivers striving to offer their babies the best nutrition possible.", attributes: [NSAttributedString.Key.font :  UIFont(name: "Arial", size: 17)!])

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the background of the navigation bar to our custom image
        let image = UIImage(named: "NavigationBarBackGroundTall2.png")!
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        //Set the background of the View to our custom Background.png
        self.view.addBackground(image: "About Us New2.png")
        
        //Set the font and size of text in the navigation bar
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "ArialRoundedMTBold", size: 20)!]
        
        self.revealViewController().rearViewRevealWidth = 150

        if self.revealViewController() != nil {
            menuBarButton.target = self.revealViewController()
            menuBarButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        //Round the corners of the images
        image2.layer.cornerRadius = 5.0
        image2.clipsToBounds = true
        image1.layer.cornerRadius = 5.0
        image1.clipsToBounds = true
        
        aboutUsLabel.attributedText = aboutUsMessage
        scrollView.contentInsetAdjustmentBehavior = .never

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

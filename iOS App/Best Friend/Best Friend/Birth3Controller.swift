//
//  Birth3Controller.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 1/15/18.
//  Copyright © 2018 H & H Solutions. All rights reserved.
//

import UIKit

class Birth3Controller: UIViewController {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let boldFont = UIFont(name: "Arial-BoldMT", size: 32.0)!
        
        let title = NSMutableAttributedString(string: "Birth Date")
        title.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 3, green: 157, blue: 163), range: NSRange(location: 0, length: title.length))
        title.addAttribute(NSAttributedString.Key.font, value: boldFont, range: NSRange(location: 0, length: title.length))
        
        let formattedMessage: NSMutableAttributedString = title
        formattedMessage.append(NSAttributedString(string: "\n\n"))
        
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "Line2.png")
        let image1String = NSAttributedString(attachment: image1Attachment)
        formattedMessage.append(image1String)
        formattedMessage.append(NSAttributedString(string: "\n\n"))
        
        let subtitle: NSMutableAttributedString = NSMutableAttributedString(string: "Here, you can select the date of your child's birth. After this, you're all set to continue receiving articles daily and watching your little blessing grow!")
        let defaultFontSize = UIFont(name: "Arial", size: 17.0)!
        subtitle.addAttribute(NSAttributedString.Key.font, value: defaultFontSize, range: NSRange(location: 0, length: subtitle.length))
        
        formattedMessage.append(subtitle)
        
        textView.attributedText = formattedMessage
        
        // Do any additional setup after loading the view.
        self.view.addBackground(image: "Background3.png")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func letsGoDidGetPressed(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: updateProfileAfterBirthKey), object: self)
        print("Sent notification from letsGoDidGetPressed")
        
        if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
            if let birthDate:Date = datePicker.date.set9AMTime() {
                let dateFormatter: DateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM dd, yyyy HH:mm"
                profile.birthDateString = dateFormatter.string(from: birthDate) as String
            } else {
                print("Failure in updating birth date: no date available from datePicker in Birth3Controller.swift")
            }
            
            DatabaseController.saveContext()
            
            print("Success in updating birth date")
        } else {
            print("Failure in updating birth date")
        }
        
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "finallyBornAndConfirmed")
        animateBalloons(containerView: view.self)
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) {_ in
            self.dismiss(animated: true, completion: nil)
        }
//        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

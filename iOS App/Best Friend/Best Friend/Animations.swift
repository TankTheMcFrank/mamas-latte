//
//  Animations.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 7/16/18.
//  Copyright © 2018 H & H Solutions. All rights reserved.
//  Changes Copyright © 2020 by Breastfeeding's Best Friend. All rights reserved.
//

import Foundation
import UIKit

func animateBalloons(containerView: UIView) {
    let startY = containerView.frame.height
    let blueLeft = UIImageView(frame: CGRect(x: 0, y: startY + 10, width: 100, height: 250))
    let blueRight = UIImageView(frame: CGRect(x: containerView.frame.width, y: startY + 10, width: 100, height: 250))
    let greenLeft = UIImageView(frame: CGRect(x: 75, y: startY + 10, width: 100, height: 250))
    let greenRight = UIImageView(frame: CGRect(x: containerView.frame.width - 75, y: startY + 10, width: 100, height: 250))
    let blueCenter = UIImageView(frame: CGRect(x: (containerView.frame.width / 2) - 100, y: containerView.frame.height + 10, width: 200, height: 500))
    let greenFarLeft = UIImageView(frame: CGRect(x: -150, y: startY + 100, width: 100, height: 250))
    
    blueLeft.image = UIImage(named: "BlueLeftBalloon.png")
    blueLeft.contentMode = .scaleAspectFit
    containerView.addSubview(blueLeft)
    
    blueRight.image = UIImage(named: "BlueRightBalloon.png")
    blueRight.contentMode = .scaleAspectFit
    containerView.addSubview(blueRight)
    
    greenLeft.image = UIImage(named: "GreenLeftBalloon.png")
    greenLeft.contentMode = .scaleAspectFit
    containerView.addSubview(greenLeft)
    
    greenRight.image = UIImage(named: "GreenRightBalloon.png")
    greenRight.contentMode = .scaleAspectFit
    containerView.addSubview(greenRight)
    
    greenFarLeft.image = UIImage(named: "GreenLeftBalloon.png")
    greenFarLeft.contentMode = .scaleAspectFit
    containerView.addSubview(greenFarLeft)
    
    blueCenter.image = UIImage(named: "BlueRightBalloon.png")
    blueCenter.contentMode = .scaleAspectFit
    containerView.addSubview(blueCenter)
    
    let translateTopRight = CGAffineTransform(translationX: containerView.frame.width, y: -(containerView.frame.height + 200))
    let translateTopLeft = CGAffineTransform(translationX: -(containerView.frame.width), y: -(containerView.frame.height + 200))
    let translateTopLeftHigh = CGAffineTransform(translationX: -(containerView.frame.width), y: -(containerView.frame.height * 2))
    let translateCenterHigh = CGAffineTransform(translationX: 0, y: -(containerView.frame.width))
    let scaleBig = CGAffineTransform(scaleX: 3, y: 3)
    let scaleSmall = CGAffineTransform(scaleX: 0.75, y: 0.75)
    let scaleReallyBig = CGAffineTransform(scaleX: 20, y: 20)
        
    UIView.animate(withDuration: 3.0, delay: 1.0, options: [], animations: {
        blueLeft.transform = translateTopRight.concatenating(scaleBig)
        containerView.layoutIfNeeded()
    }, completion: nil)

    UIView.animate(withDuration: 4.0, delay: 1.0, options: [], animations: {
        greenRight.transform = translateTopLeft.concatenating(scaleBig)
        containerView.layoutIfNeeded()
    }, completion: nil)
    
    UIView.animate(withDuration: 3.0, delay: 0.0, options: [], animations: {
        greenLeft.transform = translateTopRight.concatenating(scaleSmall)
        containerView.layoutIfNeeded()
    }, completion: nil)
    
    UIView.animate(withDuration: 2.0, delay: 1.0, options: [], animations: {
        blueRight.transform = translateTopLeftHigh.concatenating(scaleSmall)
        containerView.layoutIfNeeded()
    }, completion: nil)
    
    UIView.animate(withDuration: 4.0, delay: 1.0, options: [], animations: {
        blueCenter.transform = translateCenterHigh.concatenating(scaleReallyBig)
        containerView.layoutIfNeeded()
    }, completion: nil)
    
    UIView.animate(withDuration: 4.0, delay: 1.0, options: [], animations: {
        greenFarLeft.transform = translateTopRight.concatenating(scaleBig)
        containerView.layoutIfNeeded()
    }, completion: nil)
}

//
//  Extensions.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 8/25/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//  Changes Copyright © 2020 by Breastfeeding's Best Friend. All rights reserved.
//

import Foundation

//This extension makes it easier to create colors
//When you use this init, all you have to do is the the RGB values as parameters
//NOTE: alpha (how opaque it is) will be 1
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
    }
}

//This extension of Date creates a function called interval(), which allows the easy calculation of days between
//two dates
extension Date {
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
}

//This extension of Date creates a function called setTime(), which allows the easy setting of time of day
extension Date {
    public func setTime(hour: Int, min: Int, sec: Int, timeZoneAbbrev: String = "UTC") -> Date? {
        let x: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]
        let cal = Calendar.current
        var components = cal.dateComponents(x, from: self)
        
        components.timeZone = TimeZone(abbreviation: timeZoneAbbrev)
        components.hour = hour
        components.minute = min
        components.second = sec
        
        return cal.date(from: components)
    }
}

//This extension of Date creates a function called set9AMTime(), which allows the easy setting of 
//time of day to 9AM local time
extension Date {
    public func set9AMTime(timeZoneAbbrev: String = "UTC") -> Date? {
        let x: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]
        let cal = Calendar.current
        var components = cal.dateComponents(x, from: self)
        
        components.timeZone = TimeZone(abbreviation: timeZoneAbbrev)
        components.hour = 9
        components.minute = 0
        components.second = 0
        
        return cal.date(from: components)
    }
}

//This extension of UIView allows you to select an image as your background and set it to
//aspect fill
extension UIView {
    func addBackground(image: String) {
        // screen width and height:
        let width = UIScreen.main.bounds.size.width
        let height = UIScreen.main.bounds.size.height
        
        let imageViewBackground = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: width, height: height)))
        imageViewBackground.image = UIImage(named: image)
        
        // you can change the content mode:
        imageViewBackground.contentMode = UIView.ContentMode.scaleAspectFill
        
        self.addSubview(imageViewBackground)
        self.sendSubviewToBack(imageViewBackground)
    }}























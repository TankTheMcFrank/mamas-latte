//
//  NoSelectTextView.swift
//  Best Friend
//
//  Created by Frank Herring on 12/30/18.
//  Credit to Stack Overflow
//  Copyright © 2018 H & H Solutions. All rights reserved.
//

import Foundation

class NoSelectTextView: UITextView {
    override public var selectedTextRange: UITextRange? {
        get {
            return nil
        }
        set { }
    }
}

//
//  Birth2Controller.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 1/15/18.
//  Copyright © 2018 H & H Solutions. All rights reserved.
//

import UIKit

class Birth2Controller: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let boldFont = UIFont(name: "Arial-BoldMT", size: 32.0)!
        
        let title = NSMutableAttributedString(string: "All Prenatal Articles are Now Available!")
        title.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 3, green: 157, blue: 163), range: NSRange(location: 0, length: title.length))
        title.addAttribute(NSAttributedString.Key.font, value: boldFont, range: NSRange(location: 0, length: title.length))
        
        let formattedMessage: NSMutableAttributedString = title
        formattedMessage.append(NSAttributedString(string: "\n\n"))
        
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "Line2.png")
        let image1String = NSAttributedString(attachment: image1Attachment)
        formattedMessage.append(image1String)
        formattedMessage.append(NSAttributedString(string: "\n\n"))
        
        let subtitle: NSMutableAttributedString = NSMutableAttributedString(string: "Since we're welcoming your little blessing into the world, we're going to release what would have normally been all the Prenatal articles for you to read. Consequently, you may have more than just 2 unread messages available today. From here on out, you'll be receiving one new article daily to read.")
        let defaultFontSize = UIFont(name: "Arial", size: 17.0)!
        subtitle.addAttribute(NSAttributedString.Key.font, value: defaultFontSize, range: NSRange(location: 0, length: subtitle.length))
        
        formattedMessage.append(subtitle)
        
        textView.attributedText = formattedMessage
        
        //Set background image
        self.view.addBackground(image: "Background2.png")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Hide the keyboard when the return key is pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //Hide the keyboard when screen is touched
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        }, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //Ensures that the textView is scrolled all the way to the top when the view appears
    override func viewDidLayoutSubviews() {
        self.textView.setContentOffset(.zero, animated: false)
    }

}


//EXAMPLE CODE FOR MOVING THE SCREEN UPWARDS IN THE EVENT THAT THE KEYBOARD IS ABOUT TO COVER A FIELD
//    @objc func keyboardDidShow(notification: Notification) {
//        let info: NSDictionary = notification.userInfo! as NSDictionary
//        let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
//        let keyboardY = self.view.frame.size.height - keyboardSize.height
//
//        let editingTextFieldY: CGFloat! = self.activeTextField?.frame.origin.y
//
//        if self.view.frame.origin.y >= 0 {
//        //checking if the text field is really hidden behind the keyboard
//            if editingTextFieldY > keyboardY - 60 {
//                UIView.animate(withDuration: 0.25, delay: 0.0, options:     UIViewAnimationOptions.curveEaseIn, animations: {
//                    self.view.frame = CGRect(x: 0, y: self.view.frame.origin.y - (editingTextFieldY! - (keyboardY - 60)), width: self.view.bounds.width, height: self.view.bounds.height)
//                }, completion: nil)
//            }
//        }
//
//    }

//        //Add observers for moving the keyboard
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)


//
//  AppDelegate.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 3/16/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//

import CoreData
import GoogleMobileAds
import UIKit

//Notification Keys
//Notification Key for the initial set up of the profile object
let updateInitialProfileKey = "com.H&HSolutions.updateInitialProfileKey"
//Notification Key for ProfileController and UpdateProfileViewController to communicate
let updateProfileKey = "com.H&HSolutions.updateProfileKey"
//Notification Key for MessagesController to refresh the tableview when the app is reopened and new messages are available
let refreshTableViewKey = "com.H&HSolutions.refreshTableViewKey"
//Notification Key for BirthControllers to update Profile after child's birth
let updateProfileAfterBirthKey = "com.H&HSolutions.updateProfileAfterBirthKey"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //Modifies the MenuBar Buttons on each page so that they appear white instead of blue (default)
        let navigationBarAppearance = UINavigationBar.appearance()
        navigationBarAppearance.tintColor = UIColor(red: 255, green: 255, blue: 255)
        navigationBarAppearance.isTranslucent = false
        
        // Initialize the Google Mobile Ads SDK.
        // Sample AdMob app ID: ca-app-pub-3940256099942544~1458002511
        // BF's BF Admob app ID (new): ca-app-pub-2081695409566391~8180935019
        GADMobileAds.configure(withApplicationID: "ca-app-pub-2081695409566391~8180935019")

      // Required according to  - but doesn't build.
      //      GADMobileAds.sharedInstance().start(completionHandler: nil)

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        NotificationCenter.default.post(name: Notification.Name(rawValue: refreshTableViewKey), object: self)
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//        NotificationCenter.default.post(name: Notification.Name(rawValue: refreshTableViewKey), object: self)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        DatabaseController.saveContext()
    }

}


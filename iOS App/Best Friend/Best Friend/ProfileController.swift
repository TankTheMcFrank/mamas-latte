//
//  ProfileController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 3/16/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//

import CoreData
import UIKit

class ProfileController: UIViewController {

    @IBOutlet weak var menuBarButton: UIBarButtonItem!
    @IBOutlet weak var dueDateLeftLabel: UILabel!
    @IBOutlet weak var dueDateRightLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var babyIsBornButton: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the background of the navigation bar to our custom image
        let image = UIImage(named: "NavigationBarBackGroundTall2.png")!
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        
        //Set the font and size of text in the navigation bar
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "ArialRoundedMTBold", size: 20)!]
        
        self.revealViewController().rearViewRevealWidth = 150
        
        if self.revealViewController() != nil {
            menuBarButton.target = self.revealViewController()
            menuBarButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // Do any additional setup after loading the view.
        
        //Update the interface as the view loads
        updateProfileWhenNotify()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateProfileWhenNotify),
                                               name: NSNotification.Name(rawValue: updateProfileKey),
                                               object: nil)
        
        //Set background image
        self.view.addBackground(image: "Menu Background Light2.png")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func updateButtonDidGetPressed(_ sender: Any) {
        if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
            if profile.birthDateString == nil { //Baby not born
                performSegue(withIdentifier: "updateProfileSegue", sender: nil)
            } else { //Baby is born
                performSegue(withIdentifier: "updateProfileAfterBirthSegue", sender: nil)
            }
        }
    }
    
    @objc func updateProfileWhenNotify() {
        //Get the Profile object from Core Data and extra the information from it that we need
        if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
            self.title = "Baby \(profile.childName!)"
            
            //Determine whether the baby is born or not
            if profile.birthDateString == nil { //Baby is not born
                dueDateLeftLabel.text = "Due Date: "
                let index = profile.dueDateString?.index((profile.dueDateString?.startIndex)!, offsetBy: 12)
                let shortenedDateString: String = (String(profile.dueDateString![..<index!])) //Get just the date, not the time
                dueDateRightLabel.text = shortenedDateString
                babyIsBornButton.isHidden = false
            } else { //profile.birthDateString != nil, i.e. baby is born
                dueDateLeftLabel.text = "Birth Date: "
                let index = profile.birthDateString?.index((profile.birthDateString?.startIndex)!, offsetBy: 12)
                let shortenedDateString: String = (String(profile.birthDateString![..<index!])) //Get just the date, not the time
                dueDateRightLabel.text = shortenedDateString
                babyIsBornButton.isHidden = true
            }
            
            if (profile.profileImage == 0) {
                self.profileImage.image = UIImage(named: "Baby Blue.png")
            } else if (profile.profileImage == 1) {
                self.profileImage.image = UIImage(named: "Baby Grey.png")
            } else if (profile.profileImage == 2) {
                self.profileImage.image = UIImage(named: "Baby Pink.png")
            } else {    //There was an error
                self.profileImage.image = UIImage(named: "Baby Blue.png")
                print("\nThere was an error loading the profileImage in ProfileController\n")
            }
        } else {
            print("Error starting in ProfileController.swift")
            self.title = "Unknown Baby"
            self.profileImage.image = UIImage(named: "Baby Blue.png")
        }
    }
    
    @IBAction func babyIsBornIsPressed(_ sender: Any) {
        if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
            let birthDate:Date = Date().set9AMTime()!
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy HH:mm"
            profile.birthDateString = dateFormatter.string(from: birthDate) as String
            DatabaseController.saveContext()
            updateProfileWhenNotify()
            animateBalloons(containerView: view.self)
            
        } else {
            print("Error getting profile object in babyIsBornIsPressed in ProfileController.swift")
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

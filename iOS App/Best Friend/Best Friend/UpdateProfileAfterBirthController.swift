//
//  UpdateProfileAfterBirthController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 4/4/18.
//  Copyright © 2018 H & H Solutions. All rights reserved.
//

import CoreData
import UIKit

class UpdateProfileAfterBirthController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var themeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var viewInsideScrollView: UIView!
    var activeTextField: UITextField? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addBackground(image: "Background4.png")
        self.view.clipsToBounds = true
        
        if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
            self.nameTextField.delegate = self
            if profile.childName == "" {
                self.nameTextField.placeholder = "Name"
            } else {
                self.nameTextField.placeholder = profile.childName
            }
            if profile.profileImage == 0 {
                self.themeSegmentedControl.selectedSegmentIndex = 0
            } else if profile.profileImage == 1 {
                self.themeSegmentedControl.selectedSegmentIndex = 1
            } else { //profile.profileImage == 2
                self.themeSegmentedControl.selectedSegmentIndex = 2
            }
            
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy HH:mm"
            datePicker.date = dateFormatter.date(from: profile.birthDateString!)!
            
        } else {
            print("Error getting profile object in viewDidLoad in UpdateProfileController.swift")
        }
        
        //Support for hiding the keyboard when screen is touched
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tap(gesture:)))
        self.view.addGestureRecognizer(tapGesture)
        
        //Add observers for moving the keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    //Hide the keyboard when the return key is pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //Hide the keyboard when screen is touched
    @objc func tap(gesture: UITapGestureRecognizer) {
        nameTextField.resignFirstResponder()
    }
    
    @IBAction func cancelButtonDidGretPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateButtonDidGetPressed(_ sender: Any) {
        var birthDate: Date = datePicker.date
        
        birthDate = birthDate.set9AMTime()!
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy HH:mm"
        
        var image: Int
        if (themeSegmentedControl.selectedSegmentIndex == 0) {
            image = 0
        } else if (themeSegmentedControl.selectedSegmentIndex == 1) {
            image = 1
        } else if (themeSegmentedControl.selectedSegmentIndex == 2) {
            image = 2
        } else {    //There was an error (somehow) with the imageSegmentedControl
            print("Error in imageSegmentedControl in UpdateProfileAfterBirthController.swift")
            image = 1
        }
        
        if let profile = DatabaseController.getProfileObjectFromCoreData() {
            if nameTextField.text != "" { //They changed the name, so it should be changed in Core Data
                profile.childName = nameTextField.text!
            }
            profile.profileImage = Int64(image)
            profile.birthDateString = dateFormatter.string(from: birthDate)
        } else {
            print("Huge error in updateButtonDidGetPressed in UpdateProfileAfterBirthController.swift")
        }
        
        DatabaseController.saveContext()
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: updateProfileKey), object: self)
        print("Update Profile Notification Sent")
        
        //displayProfileInfoToTerminal()
        
        dismiss(animated: true, completion: nil)
    }

//    The following code (which is currently commented out) needs updating before being useful
//    //For debugging purposes, post relevant information about the profile to the terminal
//    func displayProfileInfoToTerminal() {
//        if let profile = DatabaseController.getProfileObjectFromCoreData() {
//            print("Baby Name: \(String(describing: profile.childName))")
//            print("Image Selection: \(profile.profileImage)")
//            print("Due Date: \(String(describing: profile.dueDateString))")
//            print("Birth Date: \(String(describing: profile.birthDateString))")
//        } else {
//            print("ERROR: No profile object able to be retrieved from DatabaseController in displayProfileInforToTerminal() in UpdateProfileController.swift")
//        }
//    }
    
    //Move screen upwards when typing in a text field covered by keyboard
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    }
    
    @objc func keyboardDidShow(notification: Notification) {
        let info: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        let keyboardY = self.view.frame.size.height - keyboardSize.height
        
        let editingTextFieldY: CGFloat! = self.activeTextField?.frame.origin.y
        
        if self.view.frame.origin.y >= 0 {
            //checking if the text field is really hidden behind the keyboard
            if editingTextFieldY > keyboardY - 60 {
                UIView.animate(withDuration: 0.25, delay: 0.0, options:     UIView.AnimationOptions.curveEaseIn, animations: {
                    self.view.frame = CGRect(x: 0, y: self.view.frame.origin.y - (editingTextFieldY! - (keyboardY - 60)), width: self.view.bounds.width, height: self.view.bounds.height)
                }, completion: nil)
            }
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        }, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

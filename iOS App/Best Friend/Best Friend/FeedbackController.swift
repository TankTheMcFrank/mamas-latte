//
//  FeedbackController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 7/8/18.
//  Copyright © 2018 H & H Solutions. All rights reserved.
//  Changes Copyright © 2020 by Breastfeeding's Best Friend. All rights reserved.
//

import UIKit

class FeedbackController: UIViewController {
    
    @IBOutlet weak var menuBarButton: UIBarButtonItem!
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //Set the background of the navigation bar to our custom image
        let image = UIImage(named: "NavigationBarBackGroundTall2.png")!
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        //Set the background of the View to our custom Background.png
        self.view.addBackground(image: "Background2.png")
        
        //Set the font and size of text in the navigation bar
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "ArialRoundedMTBold", size: 20)!]
        
        self.revealViewController().rearViewRevealWidth = 150

        if self.revealViewController() != nil {
            menuBarButton.target = self.revealViewController()
            menuBarButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        textView.text = "We work very hard to make this application everything you would like it to be. If you have any feedback which you'd like us to see, please click the button below. You will be taken to a Google Form where you can describe your questions, comments, or other concerns.\n\nOf course, if you love the application, please feel free to go leave a review for us on the App Store!"
    }

    @IBAction func submitFeedbackButtonDidGetPressed(_ sender: Any) {
        performSegue(withIdentifier: "feedbackSegue", sender: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

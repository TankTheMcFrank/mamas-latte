//
//  TrackerController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 3/16/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//

import UIKit

class TrackerController: UIViewController {

    @IBOutlet weak var menuBarButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set the background of the navigation bar to our custom image
        let image = UIImage(named: "NavigationBarBackGroundTall2.png")!
        self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
        
        //Set the font and size of text in the navigation bar
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "ArialRoundedMTBold", size: 20)!]
        
        self.revealViewController().rearViewRevealWidth = 150

        if self.revealViewController() != nil {
            menuBarButton.target = self.revealViewController()
            menuBarButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

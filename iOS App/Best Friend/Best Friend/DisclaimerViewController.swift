//
//  DisclaimerViewController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 5/9/18.
//  Copyright © 2018 H & H Solutions. All rights reserved.
//

import UIKit

class DisclaimerViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addBackground(image: "Background2.png")
        
        //Bold
        let boldFont = UIFont(name: "Arial-BoldMT", size: 32.0)!
        
        let title = NSMutableAttributedString(string: "A Quick Disclaimer")
        title.addAttribute(NSAttributedString.Key.font, value: boldFont, range: NSRange(location: 0, length: title.length))
        title.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 3, green: 157, blue: 163), range: NSRange(location: 0, length: title.length))
        
        let message: NSMutableAttributedString = title
        message.append(NSAttributedString(string: "\n\n"))
        
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "Line2.png")
        let image1String = NSAttributedString(attachment: image1Attachment)
        message.append(image1String)
        message.append(NSAttributedString(string: "\n\n"))
        
        let font = UIFont(name: "Arial", size: 17.0)!
        let disclaimer: NSMutableAttributedString = NSMutableAttributedString(string: "As you will soon notice, there are a couple of ads placed throughout the application. We've designed them so as to not be intrusive or disruptive to the user experience.\n\nJust like many other mobile applications, we use a third-party service to generate our advertisements. While we have moderate control over what ads appear, we do not have absolute control.\n\nWe want to let you know that we do not support the use of baby formula. This application is whole-heartedly about the education and endorsement of breastfeeding. Therefore, if you see an advertisement on your screen having to do with anything regarding baby formula or contrary to the promotion of breastfeeding, please know that we don't intend to promote their product. We ask that you please submit an email through the \"Feedback\" section of the application if you encounter this issue so that we will know to modify our ad settings.")
        disclaimer.addAttribute(NSAttributedString.Key.font, value: font, range: NSRange(location: 0, length: disclaimer.length))
        
        message.append(disclaimer)
        
        textView.attributedText = message

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Ensures that the textView is scrolled all the way to the top when the view appears
    override func viewDidLayoutSubviews() {
        self.textView.setContentOffset(.zero, animated: false)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  OnboardingViewController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 12/27/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//

import UIKit

class BirthiewController : UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController.isKind(of: Birth3Controller.self) {
//            NotificationCenter.default.post(name: Notification.Name(rawValue: updateInitialProfileKey), object: self)
            return getStepOne()
        } else if viewController.isKind(of: Birth2Controller.self) {
            return getStepZero()
        } else {
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController.isKind(of: Birth1Controller.self) {
            return getStepOne()
        } else if viewController.isKind(of: Birth2Controller.self) {
            return getStepTwo()
        } else {
            return nil
        }
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 3
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getStepZero() -> Birth1Controller {
        return storyboard!.instantiateViewController(withIdentifier: "Birth1") as! Birth1Controller
    }
    
    func getStepOne() -> Birth2Controller {
        return storyboard!.instantiateViewController(withIdentifier: "Birth2") as! Birth2Controller
    }
    
    func getStepTwo() -> Birth3Controller {
        return storyboard!.instantiateViewController(withIdentifier: "Birth3") as! Birth3Controller
    }
    
    override func viewDidLoad() {
        view.backgroundColor = .darkGray
        dataSource = self as UIPageViewControllerDataSource
        setViewControllers([getStepZero()], direction: .forward, animated: false, completion: nil)
        print("Loading BirthViewController")
        
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(createNewProfileWhenNotify),
//                                               name: NSNotification.Name(rawValue: updateInitialProfileKey),
//                                               object: nil)
        
//        //Create new baby profile object and store it in Core Data
//        //This code will create an array of Int to keep track of which messages are read
//        var newReadMessageArray = [Int]()
//        var i: Int = 0;
//        while i < 500 {
//            newReadMessageArray.append(0)
//            i = i + 1
//        }
//
//        DatabaseController.createProfileObject(profileName: "Name not assigned", imageSelection: 0, profileDueDate: "Due date not assigned", profileReadMessageArray: newReadMessageArray)
    }
    
    //This function gets called when a notification needs to be sent
//    @objc func createNewProfileWhenNotify() {
//        //print("Sent notification from WelcomePageViewController")
//        print("Received notification in Onboarding and could have done damage")
//    }
    
}


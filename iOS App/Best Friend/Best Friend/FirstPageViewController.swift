//
//  FirstPageViewController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 9/3/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//

import UIKit
import UserNotifications

class FirstPageViewController: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let welcomeMessage: DailyMessage = DailyMessage.parseWelcomeMessage()
        
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Welcome1.png")!)
        self.view.addBackground(image: "Background1.png")
        
        //Bold
        let boldFont = UIFont(name: "Arial-BoldMT", size: 32.0)!
        
        let title = NSMutableAttributedString(string: welcomeMessage.getTitle()!)
        title.addAttribute(NSAttributedString.Key.font, value: boldFont, range: NSRange(location: 0, length: title.length))
        title.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 3, green: 157, blue: 163), range: NSRange(location: 0, length: title.length))
        
        let message: NSMutableAttributedString = title
        message.append(NSAttributedString(string: "\n\n"))
        
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "Line2.png")
        let image1String = NSAttributedString(attachment: image1Attachment)
        message.append(image1String)
        message.append(NSAttributedString(string: "\n\n"))
        
        message.append(DailyMessage.constructMessage(messageIntro: welcomeMessage.getMessageIntro()!, components: welcomeMessage.getComponentsOfMessage()!, formats: welcomeMessage.getFormatting()!))
        
        textView.attributedText = message
        
        //Set up notifications
        initNotificationSetupCheck()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initNotificationSetupCheck() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound])
        { (success, error) in
            if success {
                print("Notification permission Granted")
                
                let center = UNUserNotificationCenter.current()
                
                let notification = UNMutableNotificationContent()
                notification.title = "New Articles are available!"
                notification.body = "Have you checked them out, yet?"
                notification.sound = UNNotificationSound.default
                
                var dateComponents = DateComponents()
                dateComponents.hour = 9
                dateComponents.minute = 0
                let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
                
                let request = UNNotificationRequest(identifier: UUID().uuidString, content: notification, trigger: notificationTrigger)
                center.removeAllPendingNotificationRequests()
                center.add(request)
            } else {
                print("There was a problem getting notification permission!")
            }
        }
    }
    
    //Ensures that the textView is scrolled all the way to the top when the view appears
    override func viewDidLayoutSubviews() {
        self.textView.setContentOffset(.zero, animated: false)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

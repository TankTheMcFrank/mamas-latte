//
//  OnboardingViewController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 12/27/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//

import UIKit

class OnboardingViewController : UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController.isKind(of: ThirdPageViewController.self) {
            NotificationCenter.default.post(name: Notification.Name(rawValue: updateInitialProfileKey), object: self)
            return getStepOne()
        } else if viewController.isKind(of: SecondPageViewController.self) {
            return getDisclaimer()
        } else if viewController.isKind(of: DisclaimerViewController.self) {
            return getStepZero()
        } else {
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController.isKind(of: FirstPageViewController.self) {
            return getDisclaimer()
        } else if viewController.isKind(of: DisclaimerViewController.self) {
            return getStepOne()
        } else if viewController.isKind(of: SecondPageViewController.self) {
            return getStepTwo()
        } else {
            return nil
        }
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 4
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getStepZero() -> FirstPageViewController {
        return storyboard!.instantiateViewController(withIdentifier: "FirstVC") as! FirstPageViewController
    }
    
    func getStepOne() -> SecondPageViewController {
        return storyboard!.instantiateViewController(withIdentifier: "SecondVC") as! SecondPageViewController
    }
    
    func getStepTwo() -> ThirdPageViewController {
        return storyboard!.instantiateViewController(withIdentifier: "ThirdVC") as! ThirdPageViewController
    }
    
    func getDisclaimer() -> DisclaimerViewController {
        return storyboard!.instantiateViewController(withIdentifier: "DisclaimerVC") as! DisclaimerViewController
    }
    
    override func viewDidLoad() {
        view.backgroundColor = .darkGray
        dataSource = self as UIPageViewControllerDataSource
        setViewControllers([getStepZero()], direction: .forward, animated: false, completion: nil)
        print("So... we're using the new onboarding class...")
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(createNewProfileWhenNotify),
                                               name: NSNotification.Name(rawValue: updateInitialProfileKey),
                                               object: nil)
        
        //Create new baby profile object and store it in Core Data
        //This code will create an array of Int to keep track of which messages are read
        var newReadMessageArray = [Int]()
        var i: Int = 0;
        while i < 500 {
            newReadMessageArray.append(0)
            i = i + 1
        }
        
        DatabaseController.createProfileObject(profileName: "Name not assigned", imageSelection: 0, profileDueDate: "Due date not assigned", profileReadMessageArray: newReadMessageArray)
    }
    
    //This function gets called when a notification needs to be sent
    @objc func createNewProfileWhenNotify() {
        //print("Sent notification from WelcomePageViewController")
        print("Received notification in Onboarding and could have done damage")
    }
    
}

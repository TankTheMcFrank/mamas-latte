//
//  Profile+CoreDataProperties.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 1/4/18.
//  Copyright © 2018 H & H Solutions. All rights reserved.
//
//

import Foundation
import CoreData


extension Profile {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Profile> {
        return NSFetchRequest<Profile>(entityName: "Profile")
    }

    @NSManaged public var birthDateString: String?
    @NSManaged public var birthLength: Int64
    @NSManaged public var birthWeight: Int64
    @NSManaged public var childName: String?
    @NSManaged public var dueDateString: String?
    @NSManaged public var profileImage: Int64
    @NSManaged public var readMessageArray: [Int]
    @NSManaged public var numberOfChildren: Int64
    @NSManaged public var currentWeight: Int64
    @NSManaged public var currentLength: Int64

}

//
//  MessageDetailController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 5/11/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//

import GoogleMobileAds
import UIKit

class MessageDetailController: UIViewController {
    
    
    @IBOutlet weak var textView: NoSelectTextView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var dailyMessage:DailyMessage = DailyMessage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: refreshTableViewKey), object: self)
        
        //Bold
        let boldFont = UIFont(name: "Arial-BoldMT", size: 32.0)!
        
        let title = NSMutableAttributedString(string: dailyMessage.getTitle()!)
        title.addAttribute(NSAttributedString.Key.font, value: boldFont, range: NSRange(location: 0, length: title.length))
        title.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 3, green: 157, blue: 163), range: NSRange(location: 0, length: title.length))
        
        let message: NSMutableAttributedString = title
        message.append(NSAttributedString(string: "\n\n"))
        
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "Line2.png")
        let image1String = NSAttributedString(attachment: image1Attachment)
        message.append(image1String)
        message.append(NSAttributedString(string: "\n\n"))
        message.append(DailyMessage.constructMessage(messageIntro: dailyMessage.getMessageIntro()!, components: dailyMessage.getComponentsOfMessage()!, formats: dailyMessage.getFormatting()!))
        
        textView.attributedText = message
        
        self.textView.textContainer.lineFragmentPadding = 0;
        
        let request = GADRequest()
        request.testDevices = ["b262b003e47f73bcc6bc8d83de527f9f"]
        //DON'T USE THIS ONE JUST DON'T DO IT IT'S FROM YOUR OLD ACCOUNT JUST STOPPPPPPP Production ad unit ID: ca-app-pub-2472936284634656/6878159654
        
        //NEW ID TO USE: ca-app-pub-2081695409566391/7680395653
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = self
        bannerView.load(request)
    }
    
    
    //Fix the initial layout of the message
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        textView.setContentOffset(CGPoint.zero, animated: false)
    }

}

//
//  ThirdPageViewController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 9/3/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//

import UIKit

class ThirdPageViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Welcome3.png")!)
        self.view.addBackground(image: "Background4.png")
        
        //Bold
        let boldFont = UIFont(name: "Arial-BoldMT", size: 32.0)!
        
        let title = NSMutableAttributedString(string: "Due Date")
        title.addAttribute(NSAttributedString.Key.font, value: boldFont, range: NSRange(location: 0, length: title.length))
        title.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 3, green: 157, blue: 163), range: NSRange(location: 0, length: title.length))
        
        let message: NSMutableAttributedString = title
        message.append(NSAttributedString(string: "\n\n"))
        
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "Line2.png")
        let image1String = NSAttributedString(attachment: image1Attachment)
        message.append(image1String)
        message.append(NSAttributedString(string: "\n\n"))
        
        let font = UIFont(name: "Arial", size: 17.0)!
        let disclaimer: NSMutableAttributedString = NSMutableAttributedString(string: "This helps us decide which daily articles to present to you. If your child is already born, please put his/her birth date.")
        disclaimer.addAttribute(NSAttributedString.Key.font, value: font, range: NSRange(location: 0, length: disclaimer.length))
        
        message.append(disclaimer)
        
        textView.attributedText = message
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissButtonDidGetPressed(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: updateInitialProfileKey), object: self)
        print("Sent notification from dismissButtonDidGetPressed")
        
        var birthDate: Date = datePicker.date
        
        birthDate = birthDate.set9AMTime()!
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy HH:mm"
        let dueDateString: String = dateFormatter.string(from: birthDate) as String
        
        if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
            var todaysDate:Date = Date()
            todaysDate = todaysDate.set9AMTime()!
            let numberOfDaysAwayFromBirth: Int = todaysDate.interval(ofComponent: .day, fromDate: birthDate)
            profile.dueDateString = dueDateString
            profile.birthDateString = nil
            
            if (numberOfDaysAwayFromBirth < 0) {
//                profile.dueDateString = dueDateString
//                profile.birthDateString = nil
                print("Baby is not born yet")
            } else {
//                profile.dueDateString = dueDateString
//                profile.birthDateString = nil
                print("Baby is already born")
            }

          DatabaseController.saveContext()
            
            print("Success in updating due date")
        } else {
            print("Failure in updating due date")
        }
        
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "profileSetUpComplete")
        dismiss(animated: true, completion: nil)
    }
    
    @objc func createNewProfileWhenNotify() {
        print("Received notification in Onboarding and could have done damage")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

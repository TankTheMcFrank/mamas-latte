//
//  DailyMessage.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 4/7/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//  Changes Copyright © 2020 by Breastfeeding's Best Friend. All rights reserved.
//

import CoreData
import Foundation

class DailyMessage {
    
    //Fields
    private var titleOfMessage: String
    private var messageDay: Int
    private var messageIntro: String
    private var componentsOfMessage: [String]
    private var formatting: [String]
    
    //Inits
    init() {
        titleOfMessage = "placeholder"
        messageDay = -101
        messageIntro = "placeholder"
        componentsOfMessage = []
        formatting = []
    }
    
    init(titleOfMessageIn: String, messageDayIn: Int, messageIntroIn: String, componentsOfMessageIn: [String], formattingIn: [String]) {
        titleOfMessage = titleOfMessageIn
        messageDay = messageDayIn
        messageIntro = messageIntroIn
        componentsOfMessage = componentsOfMessageIn
        formatting = formattingIn
    }
    
    
    //Getters
    func getTitle() -> String? {
        return titleOfMessage
    }
    
    func getMessageIntro() -> String? {
        return messageIntro
    }
    
    func getMessageDay() -> Int? {
        return messageDay
    }
    
    func getComponentsOfMessage() -> [String]? {
        return componentsOfMessage
    }
    
    func getFormatting() -> [String]? {
        return formatting
    }
    
    //Parses Just the WelcomeMessage from the JSON file
    public static func parseWelcomeMessage() -> DailyMessage {
        let file = Bundle.main.path(forResource: "message_data", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: file!))
        //        let jsonData = try? JSONSerialization.jsonObject(with: data!, options: []) as! [String : Any]
        let jsonData = try? JSONSerialization.jsonObject(with: data!, options: []) as? [Any]
        
        let dailyMessages = jsonData?[0] as! [String : Any]
        
        let dailyMessageComponents = dailyMessages["welcomeMessage"] as! [[String : Any]]
        
        var listOfDailyMessages: [DailyMessage] = []
        
        for group in dailyMessageComponents {
            let componentsArray: [String] = group["components"] as! [String]
            let formattingArray: [String] = group["formatting"] as! [String]
            
            listOfDailyMessages.append(DailyMessage(titleOfMessageIn: group["messageTitle"] as! String, messageDayIn: group["messageDay"] as! Int, messageIntroIn: group["messageIntro"] as! String, componentsOfMessageIn: componentsArray, formattingIn: formattingArray))
        }
        
        return listOfDailyMessages[0]
    }
    
    //Parses Just the BornMessage from the JSON file
    public static func parseBornMessage() -> DailyMessage {
        let file = Bundle.main.path(forResource: "message_data", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: file!))
        let jsonData = try? JSONSerialization.jsonObject(with: data!, options: []) as? [Any]
        
        let dailyMessages = jsonData?[2] as! [String : Any]
        
        let dailyMessageComponents = dailyMessages["bornMessage"] as! [[String : Any]]
        
        var listOfDailyMessages: [DailyMessage] = []
        
        for group in dailyMessageComponents {
            let componentsArray: [String] = group["components"] as! [String]
            let formattingArray: [String] = group["formatting"] as! [String]
            
            listOfDailyMessages.append(DailyMessage(titleOfMessageIn: group["messageTitle"] as! String, messageDayIn: group["messageDay"] as! Int, messageIntroIn: group["messageIntro"] as! String, componentsOfMessageIn: componentsArray, formattingIn: formattingArray))
        }
        
        return listOfDailyMessages[0]
    }
    
    //Parses JSON file and adds the information to a [DailyMessage]
    public static func parseJSONFile() -> [DailyMessage] {
        let file = Bundle.main.path(forResource: "message_data", ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: file!))
        let jsonData = try? JSONSerialization.jsonObject(with: data!, options: []) as? [Any]
        
        let dailyMessages = jsonData?[1] as! [String : Any]
        
        let dailyMessageComponents = dailyMessages["messages"] as! [[String : Any]]
        
        var listOfDailyMessages: [DailyMessage] = []
        
        for group in dailyMessageComponents {
            let componentsArray: [String] = group["components"] as! [String]
            let formattingArray: [String] = group["formatting"] as! [String]
            
            listOfDailyMessages.insert((DailyMessage(titleOfMessageIn: group["messageTitle"] as! String, messageDayIn: group["messageDay"] as! Int, messageIntroIn: group["messageIntro"] as! String, componentsOfMessageIn: componentsArray, formattingIn: formattingArray)), at: 0)
        }
        
        listOfDailyMessages = determineArticlesToShow(messages: listOfDailyMessages)
        
        return listOfDailyMessages
    }
    
    public static func determineArticlesToShow(messages: [DailyMessage]) -> [DailyMessage] {
        //Get the Profile's birthdate...
        //Get the Profile object from Core Data and extract the information from it that we need
        
        var dueDateString: String = ""
        
        if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
            if (profile.birthDateString == nil) {
                dueDateString = profile.dueDateString!
            } else {
                dueDateString = profile.birthDateString!
            }

            let dateFormatter:DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy HH:mm"
            
            let birthDate:Date = dateFormatter.date(from: dueDateString)!
            
            var todaysDate:Date = Date()
            
            todaysDate = todaysDate.set9AMTime()!
            
            let numberOfDays: Int = todaysDate.interval(ofComponent: .day, fromDate: birthDate)
            
            var availableMessages: [DailyMessage] = []
            
            for message in messages {
                if (profile.birthDateString == nil && numberOfDays <= 8) {
                    if (message.getMessageDay()! <= numberOfDays) {
                        availableMessages.append(message)
                    }
                } else if (profile.birthDateString == nil && numberOfDays > 8) {
                    if (message.getMessageDay()! <= 8) {
                        availableMessages.append(message)
                    }
                } else if (profile.birthDateString != nil) {
                    if (message.getMessageDay()! <= numberOfDays + 8) {
                        availableMessages.append(message)
                    }
                }
                
                //Old code which is commented out, but preserved in case of damage control
//                if (message.getMessageDay()! <= numberOfDays) {
//                    availableMessages.append(message)
//                    //print("message added: \(message)")
//                }
            }
            
            return availableMessages
        } else {
            print("Error in DailyMessage.swift")
            return [DailyMessage]()
        }
    }
    
    /**
     * This function is designed to take in multiple parameters, according to the DailyMessage class,
     * and format the information in such a fashion that the returned SpannableStringBuilder object
     * looks appropriate.
     *
     * @param messageIntro
     * @param components
     * @param formats
     * @return
     */
    public static func constructMessage(messageIntro: String, components: [String], formats: [String]) -> NSMutableAttributedString {
        let formattedMessage: NSMutableAttributedString = NSMutableAttributedString(string: messageIntro)
        let defaultFontSize = UIFont(name: "Arial", size: 17.0)!
        formattedMessage.addAttribute(NSAttributedString.Key.font, value: defaultFontSize, range: NSRange(location: 0, length: formattedMessage.length))
        
        var i: Int = 0
        while (i < components.count) {
            autoreleasepool {
                let code: String = formats[i]
                var stringToAdd: NSMutableAttributedString = NSMutableAttributedString(string: components[i])
                
                let defaultFontSize = UIFont(name: "Arial", size: 17.0)!
                stringToAdd.addAttribute(NSAttributedString.Key.font, value: defaultFontSize, range: NSRange(location: 0, length: stringToAdd.length))
                
                if (code.contains("hyperlink")) {
                    //Hyperlink
                    // You must set the formatting of the link manually
                    let linkAttributes: [NSAttributedString.Key : Any] = [
                        NSAttributedString.Key(rawValue: NSAttributedString.Key.link.rawValue): URL(string: stringToAdd.string)!,
                        NSAttributedString.Key.foregroundColor: UIColor.blue
                        ]

                    stringToAdd.addAttributes(linkAttributes, range: NSRange(location: 0, length: stringToAdd.length))
                }
                
                if (code.contains("none")) {
                    //This is effectively a placeholder...
                }
                
                if (code.contains("blue_color")) {
                    //Color blue
                    stringToAdd.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 3, green: 157, blue: 163), range: NSRange(location: 0, length: stringToAdd.length))
                }
                
                if (code.contains("red_color")) {
                    //Color red
                    stringToAdd.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red, range: NSRange(location: 0, length: stringToAdd.length))
                    print("Red Color")
                }
                
                if (code.contains("italicize")) {
                    //Italicize
                    let italicsFont = UIFont(name: "Arial-ItalicMT", size: 17.0)!
                    stringToAdd.addAttribute(NSAttributedString.Key.font, value: italicsFont, range: NSRange(location: 0, length: stringToAdd.length))
                }
                
                if (code.contains("underline")) {
                    //Underline
                    stringToAdd.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: NSRange(location: 0, length: stringToAdd.length))
                }
                
                if (code.contains("image")) {
                    //Image
                    let imageAttachment = NSTextAttachment()
                    imageAttachment.image = UIImage(named: stringToAdd.string)
                    
                    let ratio = (imageAttachment.image?.size.height)! / (imageAttachment.image?.size.width)!
                    
                    let screenSize: CGRect = UIScreen.main.bounds
                    let screenWidth = screenSize.width
                    imageAttachment.bounds = CGRect(x: 0, y: 0, width: screenWidth - 40, height: ratio * (screenWidth - 40))
                    let imageAttributedString = NSAttributedString(attachment: imageAttachment)
                    stringToAdd = imageAttributedString as! NSMutableAttributedString
                }
                
                if (code.contains("bold")) {
                    //Bold
                    let boldFont = UIFont(name: "Arial-BoldMT", size: 17.0)!
                    stringToAdd.addAttribute(NSAttributedString.Key.font, value: boldFont, range: NSRange(location: 0, length: stringToAdd.length))
                }
                
                if (code.contains("bullet_points")) {
                    //Create a bullet point
                    let bulletPoint: NSMutableAttributedString = NSMutableAttributedString(string: "\u{2022} ")
                    stringToAdd.insert(bulletPoint, at: 0)
                    
                    let paragraphStyle = createParagraphAttribute()
                    stringToAdd.addAttributes([NSAttributedString.Key.paragraphStyle: paragraphStyle], range: NSMakeRange(0, stringToAdd.length))
                    
                    stringToAdd.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 3, green: 157, blue: 163), range: NSRange(location: 0, length: stringToAdd.length))
                }
                
                formattedMessage.append(stringToAdd)
                i += 1;
            }
        }
        
        //Add some padding to the end of the message so that the text is not riding the bottom of the device's screen
        formattedMessage.append(NSMutableAttributedString(string: "\n\n"))
        return formattedMessage
    }
    
    /*
     * Utilized by the bullet point algorithm in constructMessage()
     */
    public static func createParagraphAttribute() ->NSParagraphStyle
    {
        var paragraphStyle: NSMutableParagraphStyle
        paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.tabStops = [NSTextTab(textAlignment: .left, location: 15, options: NSDictionary() as! [NSTextTab.OptionKey : Any])]
        paragraphStyle.defaultTabInterval = 8
        paragraphStyle.firstLineHeadIndent = 0
        paragraphStyle.headIndent = 8
        
        return paragraphStyle
    }
}




























//
//  SecondPageViewController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 9/3/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//

import UIKit

class SecondPageViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var genderSegmentedControl: UISegmentedControl!
    var activeTextField: UITextField? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        nameTextField.delegate = self
        
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Welcome2.png")!)
        self.view.addBackground(image: "Background3.png")
        
        //Bold
        let boldFont = UIFont(name: "Arial-BoldMT", size: 32.0)!
        
        let title = NSMutableAttributedString(string: "Create Your Baby's Profile!")
        title.addAttribute(NSAttributedString.Key.font, value: boldFont, range: NSRange(location: 0, length: title.length))
        title.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 3, green: 157, blue: 163), range: NSRange(location: 0, length: title.length))
        
        let message: NSMutableAttributedString = title
        message.append(NSAttributedString(string: "\n\n"))
        
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "Line2.png")
        let image1String = NSAttributedString(attachment: image1Attachment)
        message.append(image1String)
        message.append(NSAttributedString(string: "\n\n"))
        
        let font = UIFont(name: "Arial", size: 17.0)!
        let disclaimer: NSMutableAttributedString = NSMutableAttributedString(string: "Please note, all of your child's information is stored solely on this device, and won't be sent to any servers or seen anywhere besides this phone. Your information is safe!")
        disclaimer.addAttribute(NSAttributedString.Key.font, value: font, range: NSRange(location: 0, length: disclaimer.length))
        
        message.append(disclaimer)
        
        textView.attributedText = message
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(createNewProfileWhenNotify),
                                               name: NSNotification.Name(rawValue: updateInitialProfileKey),
                                               object: nil)
        //Add observers for moving the keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    //Hide the keyboard when the return key is pressed
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //Hide the keyboard when screen is touched
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func createNewProfileWhenNotify() {
        //Update profile object with name and gender
        if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
            profile.childName = nameTextField.text
            if genderSegmentedControl.selectedSegmentIndex == 0 {
                profile.profileImage = 0
            } else if genderSegmentedControl.selectedSegmentIndex == 1 {
                profile.profileImage = 1
            } else {    //genderSegmentedControl.selectedSegmentIndex == 2
                profile.profileImage = 2
            }
            
            DatabaseController.saveContext()
            
            print("Success in updating the profile name and profile image")
        } else {
            print("Failure to update profile name and profile image")
        }
    }
    
    //Move screen upwards when typing in a text field covered by keyboard
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    }
    
    @objc func keyboardDidShow(notification: Notification) {
        let info: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        let keyboardY = self.view.frame.size.height - keyboardSize.height
        
        let editingTextFieldY: CGFloat! = self.activeTextField?.frame.origin.y
        
        if self.view.frame.origin.y >= 0 {
            //checking if the text field is really hidden behind the keyboard
            if editingTextFieldY > keyboardY - 60 {
                UIView.animate(withDuration: 0.25, delay: 0.0, options:     UIView.AnimationOptions.curveEaseIn, animations: {
                    self.view.frame = CGRect(x: 0, y: self.view.frame.origin.y - (editingTextFieldY! - (keyboardY - 60)), width: self.view.bounds.width, height: self.view.bounds.height)
                }, completion: nil)
            }
        }
        
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        }, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  WebViewController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 7/8/18.
//  Copyright © 2018 H & H Solutions. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self

        // Do any additional setup after loading the view.
        
        let url = URL(string: "https://docs.google.com/forms/d/e/1FAIpQLSdoGa2IWx9jOhbKDpCY3K7uIl_ekNXTUlnJ_6_qY4z-logklQ/viewform")
        let request = URLRequest(url: url!)
        
        webView.load(request)
        
        webView.alpha = 0.0
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.alpha = 1.0
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

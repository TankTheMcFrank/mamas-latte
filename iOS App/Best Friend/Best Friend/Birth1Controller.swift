//
//  Birth1Controller.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 1/15/18.
//  Copyright © 2018 H & H Solutions. All rights reserved.
//

import UIKit

class Birth1Controller: UIViewController {

    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.addBackground(image: "Background1.png")
        
        let welcomeMessage: DailyMessage = DailyMessage.parseBornMessage()
        
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named:"Welcome1.png")!)
        self.view.addBackground(image: "Welcome1.png")
        
        //Bold
        let boldFont = UIFont(name: "Arial-BoldMT", size: 32.0)!
        
        let title = NSMutableAttributedString(string: welcomeMessage.getTitle()!)
        title.addAttribute(NSAttributedString.Key.font, value: boldFont, range: NSRange(location: 0, length: title.length))
        title.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 3, green: 157, blue: 163), range: NSRange(location: 0, length: title.length))
        
        let message: NSMutableAttributedString = title
        message.append(NSAttributedString(string: "\n\n"))
        
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "Line2.png")
        let image1String = NSAttributedString(attachment: image1Attachment)
        message.append(image1String)
        message.append(NSAttributedString(string: "\n\n"))
        
        message.append(DailyMessage.constructMessage(messageIntro: welcomeMessage.getMessageIntro()!, components: welcomeMessage.getComponentsOfMessage()!, formats: welcomeMessage.getFormatting()!))
        
        textView.attributedText = message
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelButtonDidGetPressed(_ sender: Any) {
        if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
            let birthDate:Date = Date().set9AMTime()!
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy HH:mm"
            profile.birthDateString = dateFormatter.string(from: birthDate) as String
            
            DatabaseController.saveContext()
            
            print("Success in updating birth date")
        } else {
            print("Failure in updating birth date")
        }
        
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "finallyBornAndConfirmed")
        animateBalloons(containerView: view)
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) {_ in
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func pastDueButtonDidGetPressed(_ sender: Any) {
        let defaults = UserDefaults.standard
        defaults.set(true, forKey: "finallyBornAndConfirmed")
        dismiss(animated: true, completion: nil)
    }
    
    //Ensures that the textView is scrolled all the way to the top when the view appears
    override func viewDidLayoutSubviews() {
        self.textView.setContentOffset(.zero, animated: false)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  MessagesCell2.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 8/19/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//

import UIKit

class MessagesCell2: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var milkDropImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

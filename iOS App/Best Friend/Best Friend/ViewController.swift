//
//  ViewController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 3/16/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.dismissLogoScreen), userInfo: nil, repeats: false)
    }
    
    func dismissLogoScreen() {
        print("Logo Dismissed")
        let transition: CATransition = CATransition()
        transition.duration = 0.4
        print("Got Here")
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        print("And Here")
        transition.type = CATransitionType.fade
        print("AND HERE")
        //self.navigationController!.view.layer.add(transition, forKey: nil)
        self.view.layer.add(transition, forKey: nil)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RevealViewController")
        self.navigationController?.pushViewController(vc!, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


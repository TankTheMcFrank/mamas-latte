//
//  DatabaseController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 5/27/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//  Changes Copyright © 2020 by Breastfeeding's Best Friend. All rights reserved.
//

import Foundation
import CoreData

class DatabaseController {
    
    private init() {
        
    }
    
    class func getContext() -> NSManagedObjectContext {
        return DatabaseController.persistentContainer.viewContext
    }
    
    class func createProfileObject(profileName: String, imageSelection: Int, profileDueDate: String, profileReadMessageArray: [Int]) {
        let initialProfile = NSEntityDescription.insertNewObject(forEntityName: "Profile", into: DatabaseController.getContext()) as! Profile
        initialProfile.dueDateString = profileDueDate
        initialProfile.childName = profileName
        initialProfile.profileImage = Int64(imageSelection)
        initialProfile.readMessageArray = profileReadMessageArray
        
        saveContext()
    }
    
    class func getProfileObjectFromCoreData() -> Profile? {
        let fetchRequest: NSFetchRequest<Profile> = Profile.fetchRequest()
        do {
            let searchResults = try DatabaseController.getContext().fetch(fetchRequest)            
            return searchResults[0]
        } catch {
            print("Error getting Profile object from Core Data")
            return nil
        }
    }
    
    class func updateReadMessageArray(updatedArray: [Int]) {
        
    }
    
    // MARK: - Core Data stack
    
    //CHANGE FROM LAZY TO STATIC
    static var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "BabyProfile")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    //ADD CLASS TO THE NEXT LINE
    class func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = persistentContainer.viewContext
}

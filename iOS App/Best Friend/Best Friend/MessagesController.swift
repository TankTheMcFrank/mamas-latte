//
//  MessagesController.swift
//  Breastfeeding's Best Friend
//
//  Created by Frank Herring on 3/16/17.
//  Copyright © 2017 H & H Solutions. All rights reserved.
//  Changes Copyright © 2020 by Breastfeeding's Best Friend. All rights reserved.
//

import CoreData
import GoogleMobileAds
import UIKit

class MessagesController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchResultsUpdating, UITextFieldDelegate, GADBannerViewDelegate {

  @IBOutlet weak var menuBarButton: UIBarButtonItem!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var textView: UITextView!
  @IBOutlet weak var bannerAdView: GADBannerView!
  let searchController = UISearchController(searchResultsController: nil)

  var dailyMessages: [DailyMessage] = []
  var filteredDailyMessages: [DailyMessage] = []
  var readMessages: [Int] = []
  var pointer: Int = 0 //Gives us the corresponding spot in readMessages to line up with indexPath.row

  override func viewDidLoad() {
    super.viewDidLoad()

    //These lines set the background of the navigation bar to our custom image
    let image = UIImage(named: "NavigationBarBackGroundTall2.png")!
    self.navigationController?.navigationBar.setBackgroundImage(image, for: .default)
    self.navigationController?.navigationBar.isOpaque = true

    //Set the font and size of text in the navigation bar
    self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.font: UIFont(name: "ArialRoundedMTBold", size: 20)!]

    //Sets how wide the side menu opens
    self.revealViewController().rearViewRevealWidth = 150

    //Necessary for menu button to work
    if self.revealViewController() != nil {
      menuBarButton.target = self.revealViewController()
      menuBarButton.action = #selector(SWRevealViewController.revealToggle(_:))
      self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }

    self.title = "Breast Feeding's BF"

    //Set up for the tableView
    tableView.dataSource = self
    tableView.delegate = self
    tableView.contentInsetAdjustmentBehavior = .never

    NotificationCenter.default.addObserver(self,
                                           selector: #selector(refreshTableView),
                                           name: NSNotification.Name(rawValue: refreshTableViewKey),
                                           object: nil)

    let request = GADRequest()
    request.testDevices = ["b262b003e47f73bcc6bc8d83de527f9f"]


    //NEW ID TO USE: ca-app-pub-2081695409566391/8231415216
    bannerAdView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
    bannerAdView.rootViewController = self
    bannerAdView.load(request)
  }

  override func viewDidAppear(_ animated: Bool) {
    tableView.insetsContentViewsToSafeArea = false

    let defaults = UserDefaults.standard
    let isSetUp = defaults.bool(forKey: "profileSetUpComplete")
    let finallyBornAndConfirmed = defaults.bool(forKey: "finallyBornAndConfirmed")
    //ADD A KEY IN HERE TO SPEED UP TABLEVIEW LOADING
    //^^Is this still necessary? I think I've already done it

    //If the profile has not yet been set up, then show the WelcomeViewController
    if !isSetUp {
      print("Not set up")
      self.tableView.isHidden = true
      self.textView.isHidden = true
      print("Attempting welcomeSegue")
      performSegue(withIdentifier: "welcomeSegue", sender: nil)
    } else if !finallyBornAndConfirmed && checkDate() {
      print("Baby finally born; asking to set up info")
      self.tableView.isHidden = true
      self.textView.isHidden = true
      print("Attempting to load BirthViewController")
      performSegue(withIdentifier: "birthSegue", sender: nil)
    } else {
      loadArticles()
    }
  }

  func loadArticles() {
    dailyMessages = DailyMessage.parseJSONFile()
    filteredDailyMessages = dailyMessages

    if (filteredDailyMessages.count == 0) {
      self.tableView.isHidden = true
      self.textView.isHidden = false

      let welcomeMessage: DailyMessage = DailyMessage.parseWelcomeMessage()

      //Bold
      let boldFont = UIFont(name: "Arial-BoldMT", size: 32.0)!

      let title = NSMutableAttributedString(string: welcomeMessage.getTitle()!)
      title.addAttribute(NSAttributedString.Key.font, value: boldFont, range: NSRange(location: 0, length: title.length))
      title.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor(red: 3, green: 157, blue: 163), range: NSRange(location: 0, length: title.length))

      let message: NSMutableAttributedString = title
      message.append(NSAttributedString(string: "\n\n"))

      let image1Attachment = NSTextAttachment()
      image1Attachment.image = UIImage(named: "Line2.png")
      let image1String = NSAttributedString(attachment: image1Attachment)
      message.append(image1String)
      message.append(NSAttributedString(string: "\n\n"))

      message.append(DailyMessage.constructMessage(messageIntro: welcomeMessage.getMessageIntro()!, components: welcomeMessage.getComponentsOfMessage()!, formats: welcomeMessage.getFormatting()!))

      textView.attributedText = message

    } else {
      self.textView.isHidden = true
      self.tableView.isHidden = false

      if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
        readMessages = profile.readMessageArray
        pointer = readMessages.count - filteredDailyMessages.count
      } else {
        print("ERROR in MessagesController.swift - viewDidAppear()")
        print("No profile object from which to grab readMessageArray")
      }

      //Programatically add the search bar using the searchController
      searchController.searchResultsUpdater = self
      searchController.hidesNavigationBarDuringPresentation = false
      searchController.dimsBackgroundDuringPresentation = false
      searchController.searchBar.placeholder = "Search for Article..."
      tableView.tableHeaderView = searchController.searchBar

      self.tableView.reloadData()
    }

  }

  //Determines number of rows in TableView
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return filteredDailyMessages.count
  }

  //Determines what goes in each cell
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = Bundle.main.loadNibNamed("MessagesCell2", owner: self, options: nil)?.first as! MessagesCell2

    cell.titleLabel.text = filteredDailyMessages[indexPath.row].getTitle()
    //print("About to Crash here")
    if readMessages[pointer + indexPath.row] == 1 {
      //print("I didn't crash yet")
      //cell.titleLabel.textColor = UIColor.gray
      cell.backgroundColor = UIColor(red: 228, green: 228, blue: 228)
    } else {
      //Intentionally left empty
    }

    if (filteredDailyMessages[indexPath.row].getMessageDay()! % 2 == 0) {
      cell.milkDropImage.image = UIImage(named: "oddOval.png")
    } else {
      cell.milkDropImage.image = UIImage(named: "evenOval.png")
    }

    //The  following three lines get the divider between TableView cells to go all the way across the TableView
    cell.preservesSuperviewLayoutMargins = false
    cell.separatorInset = UIEdgeInsets.zero
    cell.layoutMargins = UIEdgeInsets.zero

    return cell;
  }

  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    let rowHeight: Int = 120
    return CGFloat(rowHeight)
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    setMessageToRead(spotToRead: indexPath.row)
    self.searchController.isActive = false
    performSegue(withIdentifier: "messageDetailSegue", sender: filteredDailyMessages[indexPath.row])
    tableView.deselectRow(at: indexPath, animated: true)
  }

  @objc func refreshTableView() {
    filteredDailyMessages = DailyMessage.parseJSONFile()
    self.tableView.reloadData()
  }

  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "messageDetailSegue" {
      let guest = segue.destination as! MessageDetailController
      guest.dailyMessage = sender as! DailyMessage
    } else if segue.identifier == "welcomeSegue" {
      //Intentionally left empty
    } else if segue.identifier == "birthSegue" {
      //Intentionally left empy
    }
  }

  func setMessageToRead(spotToRead: Int) {
    if readMessages[pointer + spotToRead] ==  0 {
      readMessages[pointer + spotToRead] = 1

      if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
        profile.readMessageArray = readMessages
        DatabaseController.saveContext()
      } else {
        print("ERROR in MessagesController.swift - setMessageToRead()")
        print("Profile Object was not set to read and saved")
      }
    } else { //If it's not 0, it's 1, and there is nothing left to do
      //Intentionally left empty
    }
  }

  func checkDate() -> Bool {
    if let profile: Profile = DatabaseController.getProfileObjectFromCoreData() {
      let dueDateString = profile.dueDateString

      let dateFormatter: DateFormatter = DateFormatter()
      dateFormatter.dateFormat = "MMM dd, yyyy HH:mm"
      let dueDate = dateFormatter.date(from: dueDateString!)
      let todaysDate = dateFormatter.date(from: dateFormatter.string(from: Date()))

      if (todaysDate! >= dueDate!) {
        return true
      } else {
        return false
      }
    } else {
      print("ERROR in MessagesController.swift - checkDate()")
      print("No profile object from which to get due date")
      return false
    }
  }

  func updateSearchResults(for searchController: UISearchController) {
    if let searchText = searchController.searchBar.text, !searchText.isEmpty {
      filteredDailyMessages = dailyMessages.filter { article in
        return article.getTitle()!.lowercased().contains(searchText.lowercased())
      }

    } else {
      filteredDailyMessages = dailyMessages
    }
    tableView.reloadData()
  }

  //Hide the keyboard when the return key is pressed
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }

  //Hide the keyboard when screen is touched
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    self.view.endEditing(true)
  }

}
